Pong done with React
==========

Classic Pong application developed with React.
First React experiment :)

Player 1 uses Q/A.
Player 2 uses with P/L

Or use the mobile paddle.



Usage
-----

Install node modules
```
npm install
```

Usage
-----

Start server

```
gulp start-server
```


Open page in browser.
```
whatisyourip.com:3000
```

Start/restart game
```
r
```


Use mobile paddles
-----

Open page in browser
```
whatisyourip.com:3000/remote
```