var gulp = require('gulp'),
	nodemon = require('gulp-nodemon')
	gutil = require('gulp-util'),
	sass = require('gulp-sass'),
	concat = require('gulp-concat'),
	minify = require('gulp-minify-css'),
	buffer = require('vinyl-buffer'),
	uglify = require('gulp-uglify'),
	browserify = require('browserify'),
	babelify = require('babelify'),
	babel_presets = require('babel-preset-react'),
	source = require('vinyl-source-stream'),
	livereload = require('gulp-livereload');


/***********************************************
*               PONG GAME tasks                *
************************************************/

gulp.task('pong-game-build-minified-css', function () {
	gutil.log('pong-game-build-minified-css is running!')

	return gulp.src('src/pong-game/components/**/*.scss')
			.pipe(sass())
			.pipe(concat('pong-game.css'))
			.pipe(minify())
			.pipe(gulp.dest('dist/pong-game/css'))
			.pipe(livereload());
});

gulp.task('pong-game-build-js-bundle', function () {
	gutil.log('pong-game-build-js-bundle is running!')

	browserify({
		entries: 'src/pong-game/index.jsx',
		extensions: ['.jsx'],
		debug: true
	})
			.transform(babelify, {presets: ["react"], plugins: ["transform-react-display-name"]})
			.bundle()
			.pipe(source('bundle-pong-game-min.js'))
			.pipe(buffer())
			.pipe(uglify())
			.pipe(gulp.dest('dist/pong-game/js'))
			.pipe(livereload());

});


/************************************************
 *          CLIENT-REMOTE-PADDLE tasks          *
 ************************************************/
gulp.task('client-remote-paddle-build-minified-css', function () {
	gutil.log('client-remote-paddle-build-minified-css is running!')

	return gulp.src('src/client-remote-paddle/components/**/*.scss')
			.pipe(sass())
			.pipe(concat('client-remote.css'))
			.pipe(minify())
			.pipe(gulp.dest('dist/client-remote-paddle/css'))
			.pipe(livereload());
});

gulp.task('client-remote-build-js-bundle', function () {
	gutil.log('client-remote-build-js-bundle is running!')

	browserify({
		entries: 'src/client-remote-paddle/index.jsx',
		extensions: ['.jsx'],
		debug: true
	})
			.transform(babelify, {presets: ["react"], plugins: ["transform-react-display-name"]})
			.bundle()
			.pipe(source('bundle-client-remote-min.js'))
			.pipe(buffer())
			.pipe(uglify())
			.pipe(gulp.dest('dist/client-remote-paddle/js'))
			.pipe(livereload());

});


/************************************************
 *            VENDOR-js-minify tasks            *
 ************************************************/
/*gulp.task('build-vendor-js-bundle', function () {
	gutil.log('build-vendor-js-bundle is running!')

	return gulp.src('src/js/vendor/!*.*')
			.pipe(concat('vendor-min.js'))
			.pipe(uglify())
			.pipe(gulp.dest('dist/js/vendor'))
			.pipe(gulp.dest('dist-client/js/vendor'));
});

gulp.task('build-client-remote-vendor-js-bundle', function () {
	gutil.log('build-client-remote-vendor-js-bundle is running!')

	return gulp.src('client/js/vendor/!*.*')
			.pipe(concat('vendor-min.js'))
			.pipe(uglify())
			.pipe(gulp.dest('dist-client/js/vendor'));
});*/


/************************************************
 *                 SERVER tasks                 *
 ************************************************/
gulp.task('start-server', function () {
	nodemon({ script: './src/server/server.js'});
});

/************************************************
 *                  WATCH tasks                 *
 ************************************************/
gulp.task('watch-pong-game', function () {
	// livereload.listen();
	gulp.watch('src/pong-game/components/**/*.scss', ['pong-game-build-minified-css']);
	gulp.watch(['src/pong-game/*.jsx','src/pong-game/components/**/*.jsx'], ['pong-game-build-js-bundle']);
});

gulp.task('watch-client-remote-paddle', function () {
	// livereload.listen();
	gulp.watch('src/client-remote-paddle/components/**/*.scss', ['client-remote-paddle-build-minified-css']);
	gulp.watch(['src/client-remote-paddle/*.jsx','src/client-remote-paddle/components/**/*.jsx'], ['client-remote-build-js-bundle']);
});