var express = require("express");
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var path = require('path');

var player1, player2;

app.use(express.static(__dirname + "/../../dist/pong-game/"));
app.use(express.static(__dirname + "/../../dist/client-remote-paddle/"));

app.get('/', function(req, res){
	res.sendFile(__dirname + '/../../dist/pong-game/index.html');
});

app.get('/remote', function(req, res){
	res.sendFile(path.resolve(__dirname+'/../../dist/client-remote-paddle/index.html'));
});

// server handles connected player
io.on('connection', function(socket){

	// handle connection
	if(player1 === undefined){
		player1 = socket;
		console.log('Player 1 connected');
		socket.emit('connected', { player: 'player1' });
	}
	else if(player2 === undefined){
		player2 = socket;
		console.log('Player 2 connected');
		socket.emit('connected', { player: 'player2' });
	}
	else{
		console.log('Wait until game finished...lazy solution..');
	}

	socket.on('player-data', function(data){
		socket.broadcast.emit('player-data', data);
	});

	// when the user disconnects.. perform this
	socket.on('disconnect', function () {

		if (socket.player = 'player1') {
			console.log('Player 1 disconnected');
			player1 = undefined;
		}
		else if (socket.player = 'player2') {
			console.log('Player 2 disconnected');
			player2 = undefined;
		}
	});
});

http.listen(3000, function(){
	console.log('Server listening on *:3000');
});