'use strict';

var React = require('react');
var ReactDOM = require('react-dom');

var Court = require('./components/Court/Court.jsx');
var GameRunner = require('./components/GameRunner/GameRunner.jsx');


var PongApplication = React.createClass({
	render: function () {
		return <div className='pong-app'>
			<Court />
			<GameRunner />
		</div>;

	}
});

ReactDOM.render(
		<PongApplication />, document.getElementById('container')
);

