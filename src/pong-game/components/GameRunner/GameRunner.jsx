var React = require('react');

var Points = require('../Points/Points.jsx');
var Ball = require('../Ball/Ball.jsx');
var Paddle = require('../Paddle/Paddle.jsx');

var io = require('socket.io-client');


var GameRunner = React.createClass({

	getInitialState: function() {
		return{
			    pointsPlayer1:0,
				pointsPlayer2:0,
				ballPos_top:130,
				ballPos_left:100,
				ballPos_xDirection:1,
				ballPos_yDirection:1,
				paddle1_top:300,
				paddle2_top:300
		}
	},

	componentDidMount: function() {
		this.msTickInterval = 13;
		this.ySpeedPaddle = 7;

		this.isPlayer1_up_keyDown = false;
		this.isPlayer1_down_keyDown = false;

		this.isPlayer2_up_keyDown = false;
		this.isPlayer2_down_keyDown = false;

		this.isBallPaused = true;

		this.socket = io();
		this.socket.on('player-data', this.onRemoteDataReceived);

		this.ballSound = new Audio('audio/beep.wav');
		this.ballSound2 = new Audio('audio/beep-2.wav');

		document.addEventListener('keyup', this.onKeyPlayerUp);
		document.addEventListener('keydown', this.onKeyPlayerDown);

		// this.newBallInit();
		this.interval = setInterval(this.onTick, this.msTickInterval);
	},

	componentWillUnmount: function() {
		document.removeEventListener('keyup', this.onKeyPlayerUp);
		document.removeEventListener('keydown', this.onKeyPlayerDown);

		clearInterval(this.interval);
	},

	onRemoteDataReceived: function(data){
		var newTop = 530 * data.percentMoved;

		if (data.player == 'player1') {
			this.setState({paddle1_top: newTop});
		}
		else {
			this.setState({paddle2_top: newTop});
		}
	},

	newGame: function() {
		this.isBallPaused = true;

		this.setState({pointsPlayer1:0, pointsPlayer2:0, ballPos_top:130, ballPos_left:100, ballPos_xDirection:1, ballPos_yDirection:1});
		this.onStartBall();
	},

	newBallInit: function() {
		this.xSpeedBall = 10;
		this.ySpeedBall = 1;

		this.player1Fail = false;
		this.player2Fail = false;

		this.isBallPaused = false;
	},

	onTick: function() {
		this.renderBall();
		this.renderPaddles();
	},

	renderBall: function(){
		if(this.isBallPaused) return;

		var xDirectionNew = this.state.ballPos_xDirection;
		var yDirectionNew = this.state.ballPos_yDirection;

		var leftNew = this.state.ballPos_left + this.xSpeedBall * xDirectionNew;
		var topNew = this.state.ballPos_top + this.ySpeedBall * yDirectionNew;

		// change y-bounce direction
		if(topNew >= 585) {
			yDirectionNew = -1;
			topNew = 585;
			this.ballSound2.play();
		}
		else if(topNew <= 0) {
			topNew = 0
			yDirectionNew = 1;
			this.ballSound2.play();
		}


		// animate ball out of bounds
		if(this.player2Fail){
			if(leftNew >= 1000) {
				this.setPoints('1');
				xDirectionNew = -1;
				leftNew = 600;
			}
		}
		else if(this.player1Fail) {
			if(leftNew <= -200) {
				this.setPoints('2');
				xDirectionNew = 1;
				leftNew = 200;
			}
		}
		else if(leftNew >= 735 && !this.player2Fail) {
			if(this.state.ballPos_top + 15 < this.state.paddle2_top || this.state.ballPos_top > this.state.paddle2_top + 70){
				this.player2Fail = true;
			}
			else{
				this.player2Fail = false;
				xDirectionNew = (this.isPlayer2_up_keyDown || this.isPlayer2_down_keyDown) ? -1.2 : -1;
				yDirectionNew = (6 * (this.state.ballPos_top - this.state.paddle2_top) / 55) - 3;
				leftNew = 735;
				this.ballSound.play();
			}
		}
		else if(leftNew <= 50 && !this.player1Fail) {
			if(this.state.ballPos_top + 15 < this.state.paddle1_top || this.state.ballPos_top > this.state.paddle1_top + 70){
				this.player1Fail = true;
			}
			else{
				this.player1Fail = false;
				leftNew = 50
				xDirectionNew = (this.isPlayer1_up_keyDown || this.isPlayer1_down_keyDown) ? 1.2 : 1;
				yDirectionNew = (6 * (this.state.ballPos_top - this.state.paddle1_top) / 55) - 3;
				this.ballSound.play();
			}
		}

		this.setState({ballPos_top:Math.round(topNew), ballPos_left:Math.round(leftNew), ballPos_xDirection:xDirectionNew, ballPos_yDirection:yDirectionNew});
	},

	renderPaddles: function(){
		var newTop;

		// up
		if (this.isPlayer1_up_keyDown) {
			newTop = this.state.paddle1_top - this.ySpeedPaddle < 0 ? 0 : this.state.paddle1_top - this.ySpeedPaddle;
			this.setState({paddle1_top: newTop});
		}
		else if (this.isPlayer1_down_keyDown) {
			newTop = this.state.paddle1_top + this.ySpeedPaddle > 530 ? 530 : this.state.paddle1_top + this.ySpeedPaddle;
			this.setState({paddle1_top: newTop});
		}

		if (this.isPlayer2_up_keyDown) {
			newTop = this.state.paddle2_top - this.ySpeedPaddle < 0 ? 0 : this.state.paddle2_top - this.ySpeedPaddle;
			this.setState({paddle2_top: newTop});
		}
		else if (this.isPlayer2_down_keyDown) {
			newTop = this.state.paddle2_top + this.ySpeedPaddle > 530 ? 530 : this.state.paddle2_top + this.ySpeedPaddle;
			this.setState({paddle2_top: newTop});
		}
	},

	setPoints: function(player){
		var newPoints;

		this.isBallPaused = true;

		if(player == '1'){
			newPoints = this.state.pointsPlayer1 + 1;
			this.setState({pointsPlayer1:newPoints});
		}
		else{
			newPoints = this.state.pointsPlayer2 + 1;
			this.setState({pointsPlayer2:newPoints});
		}

		if(newPoints == 10){
			this.submitResult();
		}
		else{
			this.onStartBall();
		}
	},

	onStartBall: function() {
		this.waitRestart = setTimeout(this.onTickRestartBall, 2000);
	},

	onTickRestartBall: function() {
		clearTimeout(this.waitRestart);
		this.newBallInit()
	},

	onKeyPlayerDown: function(e) {
		e = e || window.event;

		// player 1
		if (e.keyCode == 81) {
			this.isPlayer1_up_keyDown = true;
			this.isPlayer1_down_keyDown = false;
		}
		else if (e.keyCode == 65) {
			this.isPlayer1_up_keyDown = false;
			this.isPlayer1_down_keyDown = true;
		}
		else if (e.keyCode == 80) { // player 2
			this.isPlayer2_up_keyDown = true;
			this.isPlayer2_down_keyDown = false;
		}
		else if (e.keyCode == 76) {
			this.isPlayer2_up_keyDown = false;
			this.isPlayer2_down_keyDown = true;
		}
	},

	onKeyPlayerUp: function(e) {
		e = e || window.event;

		// player 1
		if (e.keyCode == 81 && this.isPlayer1_up_keyDown) {
			this.isPlayer1_up_keyDown = false;
			this.isPlayer1_down_keyDown = false;
		}
		else if (e.keyCode == 65 && this.isPlayer1_down_keyDown) {
			this.isPlayer1_up_keyDown = false;
			this.isPlayer1_down_keyDown = false;
		}
		else if (e.keyCode == 80 && this.isPlayer2_up_keyDown) {
			this.isPlayer2_up_keyDown = false;
			this.isPlayer2_down_keyDown = false;
		}
		else if (e.keyCode == 76 && this.isPlayer2_down_keyDown) {
			this.isPlayer2_up_keyDown = false;
			this.isPlayer2_down_keyDown = false;
		}
		else if (e.keyCode == 82) {
			this.newGame();
		}
	},

	submitResult: function() {
		var data = {
			points1: this.state.pointsPlayer1,
			points2: this.state.pointsPlayer2
		};

		console.log('Result is submitted: ' + data);

		// Submit form via jQuery/AJAX
		/*$.ajax({    type: 'POST',
					url: '/some/url',
					data: data
				})
				.done(function(data) {
					console.log('Result posted');
				})
				.fail(function() {
					console.log('Failed to post');
				});*/
	},

	render: function () {
		return <div>
			<Points user='player1' points={this.state.pointsPlayer1} />
			<Points user='player2' points={this.state.pointsPlayer2} />
			<Ball top={this.state.ballPos_top} left={this.state.ballPos_left} />
			<Paddle user='player1' top={this.state.paddle1_top} />
			<Paddle user='player2' top={this.state.paddle2_top} />
		</div>;
	}
});

module.exports = GameRunner;
