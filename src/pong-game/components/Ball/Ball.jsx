var React = require('react');

var Ball = React.createClass({
	render: function () {
		var divStyle = {
			top: this.props.top,
			left: this.props.left
		};

		return <div style={divStyle} className='ball'></div>;
	}
});

module.exports = Ball;