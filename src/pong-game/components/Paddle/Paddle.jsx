var React = require('react');

var Paddle = React.createClass({
	render: function () {
		var divStyle = {
			top: this.props.top
		};

		var classes = 'pad ' + this.props.user;

		return <div style={divStyle} className={classes}></div>;
	}
});

module.exports = Paddle;