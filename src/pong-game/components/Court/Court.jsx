var React = require('react');

var Middle = React.createClass({
	render: function () {
		return <div className='middle'></div>;
	}
});

var Court = React.createClass({
	render: function () {
		return <div className='court'>
			<Middle />
		</div>;
	}
});

module.exports = Court;

