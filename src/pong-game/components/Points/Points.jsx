var React = require('react');

var Points = React.createClass({

	render: function () {
		var classes = 'points ' + this.props.user;

		return <div className={classes}>{this.props.points}</div>;
	}
});

module.exports = Points;