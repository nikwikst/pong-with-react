var React = require('react');
var io = require('socket.io-client');

var Paddle = React.createClass({

	getInitialState: function() {
		return {
			top: 310,
			player:''
		}
	},

	componentDidMount: function() {
		var self = this;

		this.socket = io();
		this.socket.on('connected', function(data){
			self.setState({player: data.player});
		});

		document.addEventListener('touchstart', this.onMouseDown);
		document.addEventListener('touchend', this.onMouseUp);
	},

	componentWillUnmount: function() {
		document.removeEventListener('touchstart', this.onMouseDown);
		document.removeEventListener('touchend', this.onMouseUp);
	},

	onMouseDown: function(e) {
		e.stopPropagation();
		e.preventDefault();
		document.addEventListener('touchmove', this.onMovePaddle);
	},

	onMouseUp: function(e) {
		document.removeEventListener('touchmove', this.onMovePaddle);
	},

	onMovePaddle: function(e) {
		var yPos = e.touches[0].pageY;
		start = yPos-100;

		if(yPos < 300 ) {
			yPos = 300;
		}
		else if(yPos > window.innerHeight-600){
			yPos = window.innerHeight-600;
		}

		var percentMoved = ((yPos-300) / (window.innerHeight-900)).toFixed(4);

		this.socket.emit('player-data', { player: this.state.player, percentMoved:percentMoved });

		this.setState({top: yPos});
	},

	render: function () {
		var divStyle = {
			top: this.state.top
		};

		return <div className='pad' style={divStyle}></div>;
	}
});

module.exports = Paddle;