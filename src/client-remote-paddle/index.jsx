'use strict';

var React = require('react');
var ReactDOM = require('react-dom');
var Paddle = require('./components/Paddle/Paddle.jsx');

var ClientRemotePaddleApp = React.createClass({
	render: function () {
		return <div className='pong-client-app'>
			<Paddle />
		</div>;
	}
});


ReactDOM.render(
		<ClientRemotePaddleApp />, document.getElementById('container')
);
